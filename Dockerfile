FROM mcr.microsoft.com/playwright:focal
COPY . /app
WORKDIR /app
RUN apt update
RUN apt install python3-pip
RUN pip install -r requirements.txt -i https://mirrors.aliyun.com/pypi/simple/
RUN python3 -m playwright install
CMD [ "python3","app.py" ]